import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Trending extends StatelessWidget {
  const Trending({Key? key}) : super(key: key);



  @override
  Widget build(BuildContext context) {
    bool isButtonPressed = false;
    return Scaffold(
      backgroundColor: Colors.white,

      body: Stack(

        children: [
          SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(padding: EdgeInsets.only(left: 15, top: 60),
                    child: Text('Trending',
                      style: TextStyle(fontSize: 35,fontWeight: FontWeight.bold,color: Colors.black),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 15, top: 15),
                    height: 60,
                    child: ListView(
                      scrollDirection: Axis.horizontal,
                      children: [

                        Container(

                          margin: EdgeInsets.only(left: 10, top: 10, right: 10),
                          width: 100,
                          //height: 50,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(20),
                              topRight: Radius.circular(20),
                              bottomLeft: Radius.circular(20),
                              bottomRight: Radius.circular(20),
                            ),

                          ),

                            child:ElevatedButton(
                              child: Text('All'),
                              onPressed: () {
                              },
                              style: ButtonStyle(
                                backgroundColor: MaterialStateProperty.resolveWith<Color>(
                                      (Set<MaterialState> states) {
                                    if (states.contains(MaterialState.pressed)) return Colors.pinkAccent;
                                    return Colors.grey;
                                  },
                                ),
                              ),
                            )

                        ),



                        Container(

                          margin: EdgeInsets.only(left: 10, top: 10, right: 10),
                          width: 100,
                          decoration: BoxDecoration(
                            color: Colors.green,
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(20),
                              topRight: Radius.circular(20),
                              bottomLeft: Radius.circular(20),
                              bottomRight: Radius.circular(20),
                            ),


                          ),
                            child:RaisedButton(
                              child:Text("Rock",
                                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                              ),
                              onPressed: (
                                  ) {
                              },
                            )

                          // color: Colors.purple[600],
                          //child: const Center(child: Text('Item 1', style: TextStyle(fontSize: 18, color: Colors.white),)),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 10, top: 10, right: 10),
                          width: 110,
                          decoration: BoxDecoration(
                            color: Colors.green,
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(10),
                              topRight: Radius.circular(10),
                              bottomLeft: Radius.circular(10),
                              bottomRight: Radius.circular(10),
                            ),

                          ),
                          //color: Colors.purple[600],
                            child:RaisedButton(
                              child:Text("Pop ",
                                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                              ),
                              onPressed: (

                                  ) {
                              },
                            )
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 10, top: 10, right: 10),
                          width: 110,
                          decoration: BoxDecoration(
                            color: Colors.green,
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(10),
                              topRight: Radius.circular(10),
                              bottomLeft: Radius.circular(10),
                              bottomRight: Radius.circular(10),
                            ),
                          ),
                            child:RaisedButton(
                              child:Text("Hip-Hop",
                                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                              ),
                              onPressed: (

                                  ) {
                              },
                            )
                          // color: Colors.purple[600],
                          // child: const Center(child: Text('Item 1', style: TextStyle(fontSize: 18, color: Colors.white),)),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 10, top: 10, right: 10),
                          width: 110,
                          decoration: BoxDecoration(
                            color: Colors.green,
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(10),
                              topRight: Radius.circular(10),
                              bottomLeft: Radius.circular(10),
                              bottomRight: Radius.circular(10),
                            ),

                          ),
                            child:RaisedButton(
                              child:Text("Classical",
                                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                              ),
                              onPressed: (

                                  ) {
                              },
                            )
                          // color: Colors.purple[600],
                          // child: const Center(child: Text('Item 1', style: TextStyle(fontSize: 18, color: Colors.white),)),
                        ),

                      ],

                    ),

                  ),


                  //Another= Section

                  Container(
                    margin: EdgeInsets.only(left: 15, top: 30),
                    child: Text('Top Trending - 2021',
                      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 26,
                      color: Colors.black,
                      ),
                    ),
                  ),
                  Container(
                    color: Colors.white,
                    margin: EdgeInsets.only(left: 15, top: 15),
                    height: 120,
                    child: ListView(
                      scrollDirection: Axis.horizontal,
                      children: [

                        Container(
                          margin: EdgeInsets.only(left: 10, top: 10, right: 10),
                          width: 110,
                          height: 150,
                          decoration: BoxDecoration(
                            color: Colors.green,
                            borderRadius: BorderRadius.only(

                              topLeft: Radius.circular(10),
                              topRight: Radius.circular(10),
                              bottomLeft: Radius.circular(10),
                              bottomRight: Radius.circular(10),
                            ),
                            image: DecorationImage(
                              fit: BoxFit.fill,
                              image: NetworkImage("https://i.ytimg.com/vi/FxmkqpOgzG8/maxresdefault.jpg"),
                            ),
                          ),
                        ),
                        Container(

                          margin: EdgeInsets.only(left: 25, top: 28),
                          child: Row(
                            children: [
                             Column(
                               children: [
                                 Text('  #1', style: TextStyle(fontSize: 20,color: Colors.black),),
                                 Text('    Proud Mary- Creedence',style: TextStyle(fontSize: 22),),
                                 Text('   Devin',style: TextStyle(fontSize: 20),),
                               ],
                             ),
                            ],
                          ),
                        ),


                      ],

                    ),

                  ),
                  Container(
                    color: Colors.white,
                    margin: EdgeInsets.only(left: 15, top: 15),
                    height: 120,
                    child: ListView(
                      scrollDirection: Axis.horizontal,
                      children: [

                        Container(
                          margin: EdgeInsets.only(left: 10, top: 10, right: 10),
                          width: 110,
                          height: 150,
                          decoration: BoxDecoration(
                            color: Colors.green,
                            borderRadius: BorderRadius.only(

                              topLeft: Radius.circular(10),
                              topRight: Radius.circular(10),
                              bottomLeft: Radius.circular(10),
                              bottomRight: Radius.circular(10),
                            ),
                            image: DecorationImage(
                              fit: BoxFit.fill,
                              image: NetworkImage("https://i.ytimg.com/vi/VeRHHo7HEMc/hqdefault.jpg"),
                            ),
                          ),
                        ),
                        Container(

                          margin: EdgeInsets.only(left: 25, top: 28),
                          child: Row(
                            children: [
                              Column(
                                children: [
                                  Text('  #2', style: TextStyle(fontSize: 20,color: Colors.black),),
                                  Text('    Angie- Rolling Stone',style: TextStyle(fontSize: 22),),
                                  Text('   Devin',style: TextStyle(fontSize: 20),),
                                ],
                              ),
                            ],
                          ),
                        ),


                      ],

                    ),

                  ),
                  Container(
                    color: Colors.white,
                    margin: EdgeInsets.only(left: 15, top: 15),
                    height: 120,
                    child: ListView(
                      scrollDirection: Axis.horizontal,
                      children: [

                        Container(
                          margin: EdgeInsets.only(left: 10, top: 10, right: 10),
                          width: 110,
                          height: 150,
                          decoration: BoxDecoration(
                            color: Colors.green,
                            borderRadius: BorderRadius.only(

                              topLeft: Radius.circular(10),
                              topRight: Radius.circular(10),
                              bottomLeft: Radius.circular(10),
                              bottomRight: Radius.circular(10),
                            ),
                            image: DecorationImage(
                              fit: BoxFit.fill,
                              image: NetworkImage("https://i0.wp.com/felixsimpson.com/wp-content/uploads/2021/12/1N7A4816-1024x683.jpg?ssl=1"),
                            ),
                          ),
                        ),
                        Container(

                          margin: EdgeInsets.only(left: 25, top: 28),
                          child: Row(
                            children: [
                              Column(
                                children: [
                                  Text('  #3', style: TextStyle(fontSize: 20,color: Colors.black),),
                                  Text('   Roxanne- The Police',style: TextStyle(fontSize: 22),),
                                  Text('   Devin',style: TextStyle(fontSize: 20),),
                                ],
                              ),
                            ],
                          ),
                        ),


                      ],

                    ),

                  ),
                  Container(
                    color: Colors.white,
                    margin: EdgeInsets.only(left: 15, top: 15),
                    height: 120,
                    child: ListView(
                      scrollDirection: Axis.horizontal,
                      children: [

                        Container(
                          margin: EdgeInsets.only(left: 10, top: 10, right: 10),
                          width: 110,
                          height: 150,
                          decoration: BoxDecoration(
                            color: Colors.green,
                            borderRadius: BorderRadius.only(

                              topLeft: Radius.circular(10),
                              topRight: Radius.circular(10),
                              bottomLeft: Radius.circular(10),
                              bottomRight: Radius.circular(10),
                            ),
                            image: DecorationImage(
                              fit: BoxFit.fill,
                              image: NetworkImage("https://i.pinimg.com/originals/25/41/34/254134cb1b46f0a15fed9994538342ce.jpg"),
                            ),
                          ),
                        ),
                        Container(

                          margin: EdgeInsets.only(left: 25, top: 28),
                          child: Row(
                            children: [
                              Column(
                                children: [
                                  Text('  #4', style: TextStyle(fontSize: 20,color: Colors.black),),
                                  Text('    My Sharona- Kinks',style: TextStyle(fontSize: 22),),
                                  Text('   Devin',style: TextStyle(fontSize: 20),),
                                ],
                              ),
                            ],
                          ),
                        ),


                      ],

                    ),

                  ),
                  Container(
                    color: Colors.white,
                    margin: EdgeInsets.only(left: 15, top: 15),
                    height: 120,
                    child: ListView(
                      scrollDirection: Axis.horizontal,
                      children: [

                        Container(
                          margin: EdgeInsets.only(left: 10, top: 10, right: 10),
                          width: 110,
                          height: 150,
                          decoration: BoxDecoration(
                            color: Colors.green,
                            borderRadius: BorderRadius.only(

                              topLeft: Radius.circular(10),
                              topRight: Radius.circular(10),
                              bottomLeft: Radius.circular(10),
                              bottomRight: Radius.circular(10),
                            ),
                            image: DecorationImage(
                              fit: BoxFit.fill,
                              image: NetworkImage("https://th.bing.com/th/id/R.2d029b510067d6767af28e5669bb6af0?rik=3VjeBe1nsq0a%2fw&riu=http%3a%2f%2fwww.buymixtapes.com%2fupload%2fmembers%2fimages%2f0to100mixtape.jpg&ehk=tiNcmYASkbqH1f6R8Mvp%2buXQQxXFBwmPLaK7Ysx7qzY%3d&risl=&pid=ImgRaw&r=0&sres=1&sresct=1"),
                            ),
                          ),
                        ),
                        Container(

                          margin: EdgeInsets.only(left: 25, top: 28),
                          child: Row(
                            children: [
                              Column(
                                children: [
                                  Text('  #5', style: TextStyle(fontSize: 20,color: Colors.black),),
                                  Text('   Amanda- Boston',style: TextStyle(fontSize: 22),),
                                  Text('   Devin',style: TextStyle(fontSize: 20),),
                                ],
                              ),
                            ],
                          ),
                        ),


                      ],

                    ),

                  ),


                  //Another Secation






                ],


              )
          ),


        ],

      ),


    );
  }
}
