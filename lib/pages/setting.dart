import 'package:flutter/material.dart';
class Setting extends StatelessWidget {

  const Setting({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(title: Text('Setting'),
        centerTitle: true,
        toolbarHeight: 40,
        backgroundColor: Colors.greenAccent[400],
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(360)),

      ),
      body: Center(
        child: Text('Setting',
          style: TextStyle(fontSize: 20),
        ),
      ),

    );
  }
}
