import 'package:flutter/material.dart';


class Search extends StatelessWidget {
  const Search({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: Colors.white,
     appBar: AppBar(
        shadowColor: Colors.white70,
       toolbarHeight: 120,
       elevation: 0,

       //shadowColor: Colors.white,
       backgroundColor: Colors.white,
       title: Container(
         margin: EdgeInsets.only(top: 20),

         width: 600,
         height: 50,
         decoration: BoxDecoration(
           color: Colors.grey, borderRadius: BorderRadius.circular(40),
         ),

         child: TextField(
           cursorColor: Colors.white,
           style: TextStyle(fontSize: 20, color: Colors.white),
           decoration: InputDecoration(
             focusColor: Colors.white70,
             hintText: 'Searching for Songs,albums',fillColor: Colors.white70,
             prefixIcon: Icon(Icons.search,color: Colors.white70),



           ),
         ),

       ),

     ),

    );
  }
}
