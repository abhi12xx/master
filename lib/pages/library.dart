import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
class Library extends StatelessWidget {

  const Library({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        backgroundColor: Colors.white,
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(padding: EdgeInsets.only(left: 15, top: 90),
                  child: Text('Library',
                    style: TextStyle(fontSize: 35,fontWeight: FontWeight.bold,color: Colors.black),
                  ),
                ),

                Container(
                  child: Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 20, top: 30),
                        width: 190,
                        height: 190,
                        //color: Colors.green,
                        decoration: BoxDecoration(
                          // color: Colors.green,
                          borderRadius: BorderRadius.only(

                            topLeft: Radius.circular(20),
                            topRight: Radius.circular(20),
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20),
                          ),
                          image: DecorationImage(
                            fit: BoxFit.fill,
                            image: NetworkImage("https://static01.nyt.com/images/2020/07/19/arts/15LIST-SAMPLES1/merlin_174587397_553c87c9-dff4-4348-a6e5-d55f018e98a1-superJumbo.jpg"),
                          ),
                        ),



                        child:
                        Container(
                          margin: EdgeInsets.only(top: 155, left: 15),
                          child: Text('Liked Songs', style: TextStyle(fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold),),

                        ),


                      ),
                      Container(
                        margin: EdgeInsets.only(left: 20, top: 20),
                        width: 190,
                        height: 190,
                        //color: Colors.green,
                        decoration: BoxDecoration(
                          // color: Colors.green,
                          borderRadius: BorderRadius.only(

                            topLeft: Radius.circular(20),
                            topRight: Radius.circular(20),
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20),
                          ),
                          image: DecorationImage(
                            fit: BoxFit.fill,
                            image: NetworkImage("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSDnCz5-Ow0nEEUMY65cyPd1D1eWr6lC4JPrmVaQZZqLw6tYOuRNxS9_uUkzDv4LdRfjQM&usqp=CAU"),
                          ),
                        ),



                        child:
                        Container(
                          margin: EdgeInsets.only(top: 155, left: 15),
                          child: Text('Liked Podcasts', style: TextStyle(fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold),),

                        ),


                      ),

                    ],

                  ),
                ),
                Container(
                  child: Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 20, top: 20),
                        width: 190,
                        height: 190,
                        //color: Colors.green,
                        decoration: BoxDecoration(
                          // color: Colors.green,
                          borderRadius: BorderRadius.only(

                            topLeft: Radius.circular(20),
                            topRight: Radius.circular(20),
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20),
                          ),
                          image: DecorationImage(
                            fit: BoxFit.fill,
                            image: NetworkImage("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR9w6MuK0AG-CVxs-LrxGzWTKIW7yPo4tXx1YkPlM_xS0RTXN-GFqWhlwU6pJVCCd1MyMc&usqp=CAU"),
                          ),
                        ),



                        child:
                        Container(
                          margin: EdgeInsets.only(top: 155, left: 15),
                          child: Text('Chill', style: TextStyle(fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold),),

                        ),




                      ),
                      Container(
                        margin: EdgeInsets.only(left: 20, top: 20),
                        width: 190,
                        height: 190,
                       // color: Colors.green,
                        decoration: BoxDecoration(
                          // color: Colors.green,
                          borderRadius: BorderRadius.only(

                            topLeft: Radius.circular(20),
                            topRight: Radius.circular(20),
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20),
                          ),
                          image: DecorationImage(
                            fit: BoxFit.fill,
                            image: NetworkImage("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTb4y-8m-Z4tljJ28Vr8ii40svb91WtCv1SPYkp17l0UMiYLlqkJjUiVeXtpEo4qvEB2aA&usqp=CAU"),
                          ),
                        ),



                        child:
                        Container(
                          margin: EdgeInsets.only(top: 155, left: 15),
                          child: Text('Party', style: TextStyle(fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold),),

                        ),


                      ),

                    ],

                  ),
                ),
                Container(
                  child: Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 20, top: 20),
                        width: 190,
                        height: 190,
                      //  color: Colors.green,
                        decoration: BoxDecoration(
                          // color: Colors.green,
                          borderRadius: BorderRadius.only(

                            topLeft: Radius.circular(20),
                            topRight: Radius.circular(20),
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20),
                          ),
                          image: DecorationImage(
                            fit: BoxFit.fill,
                            image: NetworkImage("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSoR-SPc-yiFrAV7KbjV7Ee70p-wkea8Wb0hnpE1XxaT2CfWWmp0AwO3uxslFL4B6MwUaU&usqp=CAU"),
                          ),
                        ),



                        child:
                        Container(
                          margin: EdgeInsets.only(top: 155, left: 15),
                          child: Text('BGM', style: TextStyle(fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold),),

                        ),


                      ),
                      Container(
                        margin: EdgeInsets.only(left: 20, top: 20),
                        width: 190,
                        height: 190,
                        //color: Colors.green,
                        decoration: BoxDecoration(
                          // color: Colors.green,
                          borderRadius: BorderRadius.only(

                            topLeft: Radius.circular(20),
                            topRight: Radius.circular(20),
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20),
                          ),
                          image: DecorationImage(
                            fit: BoxFit.fill,
                            image: NetworkImage("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT73uavuyzxcckQ4T5cRGY9OgHvqjxrQlmxlsI6CdC_JXR71Wl-ZRtbci_pfRJtZCWDKI8&usqp=CAU"),
                          ),
                        ),



                        child:
                        Container(
                          margin: EdgeInsets.only(top: 155, left: 15),
                          child: Text('Gym', style: TextStyle(fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold),),
                        ),

                      ),

                    ],

                  ),
                ),
                Container(
                  child: Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 20, top: 20),
                        width: 190,
                        height: 190,
                        //color: Colors.green,
                        decoration: BoxDecoration(
                          // color: Colors.green,
                          borderRadius: BorderRadius.only(

                            topLeft: Radius.circular(20),
                            topRight: Radius.circular(20),
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20),
                          ),
                          image: DecorationImage(
                            fit: BoxFit.fill,
                            image: NetworkImage("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQLgxPev2MjpY5hszRObRsVjBioTjV7IoxgvivZNtZKAIx-oiaGWy6LbYtGpy-RkXzrOI8&usqp=CAU"),
                          ),
                        ),



                        child:
                        Container(
                          margin: EdgeInsets.only(top: 155, left: 15),
                          child: Text('Workout Hits', style: TextStyle(fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold),),

                        ),

                      ),
                      Container(
                        margin: EdgeInsets.only(left: 20, top: 20),
                        width: 190,
                        height: 190,
                       // color: Colors.green,
                        decoration: BoxDecoration(
                          // color: Colors.green,
                          borderRadius: BorderRadius.only(

                            topLeft: Radius.circular(20),
                            topRight: Radius.circular(20),
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20),
                          ),
                          image: DecorationImage(
                            fit: BoxFit.fill,
                            image: NetworkImage("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSUtbER2RtW2sUFBG-b_oX4GleimQ5kNCoY7EHS1Q6GuylCCniPaGgMJm08DPWiwfKwlLQ&usqp=CAU"),
                          ),
                        ),



                        child:
                        Container(
                          margin: EdgeInsets.only(top: 155, left: 15),
                          child: Text('New Songs', style: TextStyle(fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold),),

                        ),

                      ),

                    ],

                  ),
                ),
                Container(
                  child: Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 20, top: 20),
                        width: 190,
                        height: 190,
                        //color: Colors.green,
                        decoration: BoxDecoration(
                          // color: Colors.green,
                          borderRadius: BorderRadius.only(

                            topLeft: Radius.circular(20),
                            topRight: Radius.circular(20),
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20),
                          ),
                          image: DecorationImage(
                            fit: BoxFit.fill,
                            image: NetworkImage("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTW0H7G1Xr2cFLbQh1Xo0D2WvmtUEL1teS4I1od4UZ6_pGhkUuWgY91Vk6LCOMEXQ_7G20&usqp=CAU"),
                          ),
                        ),



                        child:
                        Container(
                          margin: EdgeInsets.only(top: 155, left: 15),
                          child: Text('Old Songs', style: TextStyle(fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold),),

                        ),


                      ),
                      Container(
                        margin: EdgeInsets.only(left: 20, top: 20),
                        width: 190,
                        height: 190,
                       // color: Colors.green,
                        decoration: BoxDecoration(
                          // color: Colors.green,
                          borderRadius: BorderRadius.only(

                            topLeft: Radius.circular(20),
                            topRight: Radius.circular(20),
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20),
                          ),
                          image: DecorationImage(
                            fit: BoxFit.fill,
                            image: NetworkImage("https://miro.medium.com/max/798/1*s_SJB5PIDXgAVpkv0Eb8-Q.png"),
                          ),
                        ),



                        child:
                        Container(
                          margin: EdgeInsets.only(top: 135, left: 20),
                          child: Text('Liked Songs', style: TextStyle(fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold),),

                        ),

                      ),

                    ],

                  ),
                ),






              ],
            ),
          ),
        ],

      ),
    );
  }
}
