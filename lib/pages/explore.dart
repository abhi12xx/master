import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Explore extends StatelessWidget {
  const Explore({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,

      body: Stack(

        children: [
          SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(padding: EdgeInsets.only(left: 15, top: 60),
                    child: Text('Explore',
                      style: TextStyle(fontSize: 35,fontWeight: FontWeight.bold,color: Colors.black),
                    ),
                  ),
                  Padding(padding: EdgeInsets.only(top: 22, left: 15),
                    child: Text('Recently played',
                      style: TextStyle(fontSize: 22,fontWeight: FontWeight.bold,color: Colors.black),
                    ),
                  ),

                 Container(
                   margin: EdgeInsets.only(left: 15, top: 15),
                  height: 170,
                   child: ListView(
                     scrollDirection: Axis.horizontal,
                     children: [

                       Container(
                         margin: EdgeInsets.only(left: 10, top: 10, right: 10),
                         width: 110,
                         height: 150,
                         decoration: BoxDecoration(
                           color: Colors.green,
                           borderRadius: BorderRadius.only(

                             topLeft: Radius.circular(10),
                             topRight: Radius.circular(10),
                             bottomLeft: Radius.circular(10),
                             bottomRight: Radius.circular(10),
                           ),
                            image: DecorationImage(
                            fit: BoxFit.fill,
                              image: NetworkImage("https://d12swbtw719y4s.cloudfront.net/images/fQjcOVwV/ZCrXl8S8bqODMJLV3czr/7c3hJGtF8O.jpeg?w=1200"),
                         ),
                         ),
                       ),



                       Container(

                         margin: EdgeInsets.only(left: 10, top: 10, right: 10),
                         width: 110,
                         decoration: BoxDecoration(
                           color: Colors.green,
                           borderRadius: BorderRadius.only(
                             topLeft: Radius.circular(10),
                             topRight: Radius.circular(10),
                             bottomLeft: Radius.circular(10),
                             bottomRight: Radius.circular(10),
                           ),
                           image: DecorationImage(
                             fit: BoxFit.fill,

                             image: NetworkImage("https://th.bing.com/th/id/OIP.6JyonnJy8ePoXyAXzjZL3gHaHa?pid=ImgDet&rs=1"),
                           ),

                         ),
                        // color: Colors.purple[600],
                         //child: const Center(child: Text('Item 1', style: TextStyle(fontSize: 18, color: Colors.white),)),
                       ),
                       Container(
                         margin: EdgeInsets.only(left: 10, top: 10, right: 10),
                         width: 110,
                         decoration: BoxDecoration(
                           color: Colors.green,
                           borderRadius: BorderRadius.only(
                             topLeft: Radius.circular(10),
                             topRight: Radius.circular(10),
                             bottomLeft: Radius.circular(10),
                             bottomRight: Radius.circular(10),
                           ),
                           image: DecorationImage(
                             fit: BoxFit.fill,
                             image: NetworkImage("https://th.bing.com/th/id/R.a5d3383b3f9029b18f41ee3a2053b888?rik=VbyigwAa%2fyglFw&riu=http%3a%2f%2fwebdesignledger.com%2fwp-content%2fuploads%2f2015%2f05%2f7-creative-music-album-covers.jpg&ehk=ZZJvBbjFGOjqbQxnB6VD4rbWilM%2bNoQVnt2YufQ4XJ8%3d&risl=&pid=ImgRaw&r=0"),
                           ),
                         ),
                         //color: Colors.purple[600],
                         child: const Center(
                             child: Text(
                               'Item 1', style: TextStyle(
                                 fontSize: 18, color: Colors.red),),

                         ),
                       ),
                       Container(
                         margin: EdgeInsets.only(left: 10, top: 10, right: 10),
                         width: 110,
                         decoration: BoxDecoration(
                           color: Colors.green,
                           borderRadius: BorderRadius.only(
                             topLeft: Radius.circular(10),
                             topRight: Radius.circular(10),
                             bottomLeft: Radius.circular(10),
                             bottomRight: Radius.circular(10),
                           ),
                           image: DecorationImage(
                             fit: BoxFit.fill,
                             image: NetworkImage("https://i.etsystatic.com/25692110/r/il/1be3e6/3071407436/il_fullxfull.3071407436_pmw5.jpg"),
                           ),
                         ),
                        // color: Colors.purple[600],
                        // child: const Center(child: Text('Item 1', style: TextStyle(fontSize: 18, color: Colors.white),)),
                       ),
                       Container(
                         margin: EdgeInsets.only(left: 10, top: 10, right: 10),
                         width: 110,
                         decoration: BoxDecoration(
                           color: Colors.green,
                           borderRadius: BorderRadius.only(
                             topLeft: Radius.circular(10),
                             topRight: Radius.circular(10),
                             bottomLeft: Radius.circular(10),
                             bottomRight: Radius.circular(10),
                           ),
                           image: DecorationImage(
                             fit: BoxFit.fill,

                             image: NetworkImage("https://d12swbtw719y4s.cloudfront.net/images/fQjcOVwV/bv6JyrD1JbPUg72cpzmj/Hru1FBaQ2F.jpeg?w=1180"),
                           ),
                         ),
                        // color: Colors.purple[600],
                        // child: const Center(child: Text('Item 1', style: TextStyle(fontSize: 18, color: Colors.white),)),
                       ),
                       Container(
                         margin: EdgeInsets.only(left: 10, top: 10, right: 10),
                         width: 110,
                         decoration: BoxDecoration(
                           color: Colors.green,
                           borderRadius: BorderRadius.only(
                             topLeft: Radius.circular(10),
                             topRight: Radius.circular(10),
                             bottomLeft: Radius.circular(10),
                             bottomRight: Radius.circular(10),
                           ),
                           image: DecorationImage(
                             fit: BoxFit.fill,

                             image: NetworkImage("https://th.bing.com/th/id/R.fd8194b9a865679fd43f2fb582ddaa35?rik=iArj%2fB5Bp%2f2bDg&riu=http%3a%2f%2fwallpapercave.com%2fwp%2fuYeAzcK.jpg&ehk=0MgCu0KVVElUM4k2zomO5Gjvj5D1%2f6mXGY5mrPLv87I%3d&risl=&pid=ImgRaw&r=0"),
                           ),
                         ),
                         // color: Colors.purple[600],
                         // child: const Center(child: Text('Item 1', style: TextStyle(fontSize: 18, color: Colors.white),)),
                       ),
                       Container(
                         margin: EdgeInsets.only(left: 10, top: 10, right: 10),
                         width: 110,
                         decoration: BoxDecoration(
                           color: Colors.green,
                           borderRadius: BorderRadius.only(
                             topLeft: Radius.circular(10),
                             topRight: Radius.circular(10),
                             bottomLeft: Radius.circular(10),
                             bottomRight: Radius.circular(10),
                           ),
                           image: DecorationImage(
                             fit: BoxFit.fill,

                             image: NetworkImage("https://images.designtrends.com/wp-content/uploads/2016/11/02150202/Music-Album-Cover-Design.jpg"),
                           ),
                         ),
                         // color: Colors.purple[600],
                         //child: const Center(child: Text('Item 1', style: TextStyle(fontSize: 18, color: Colors.white),)),
                       ),
                       Container(
                         margin: EdgeInsets.only(left: 10, top: 10, right: 10),
                         width: 110,
                         decoration: BoxDecoration(
                           color: Colors.green,
                           borderRadius: BorderRadius.only(
                             topLeft: Radius.circular(10),
                             topRight: Radius.circular(10),
                             bottomLeft: Radius.circular(10),
                             bottomRight: Radius.circular(10),
                           ),
                           image: DecorationImage(
                             fit: BoxFit.fill,
                             image: NetworkImage("https://mir-s3-cdn-cf.behance.net/project_modules/fs/0ec5c569226647.5b79b483096c1.jpg"),
                           ),
                         ),
                         // color: Colors.purple[600],
                         // child: const Center(child: Text('Item 1', style: TextStyle(fontSize: 18, color: Colors.white),)),
                       ),

                     ],

                   ),

                 ),


                  //Another= Section

                  Container(
                    margin: EdgeInsets.only(left: 15, top: 40),
                    child: Text('Recommeded for you',
                      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
                    ),
                  ),

                  Container(
                    margin: EdgeInsets.only(left: 10, top: 10),
                    height: 170,
                    child: ListView(
                      scrollDirection: Axis.horizontal,
                      children: [
                        Container(
                          margin: EdgeInsets.only(left: 10, top: 10, right: 10),
                          width: 170,
                          decoration: BoxDecoration(
                            color: Colors.green,
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(10),
                              topRight: Radius.circular(10),
                              bottomLeft: Radius.circular(10),
                              bottomRight: Radius.circular(10),
                            ),
                            image: DecorationImage(
                              fit: BoxFit.fill,
                              image: NetworkImage("https://www.picclickimg.com/d/l400/pict/265534858659_/Reproduction-Chet-Baker-Valentine-Album-Cover-Poster-Size.jpg"),
                            ),

                          ),
                          // color: Colors.purple[600],
                          // child: const Center(child: Text('Item 1', style: TextStyle(fontSize: 18, color: Colors.white),)),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 10, top: 10, right: 10),
                          width: 170,
                          decoration: BoxDecoration(
                            color: Colors.green,
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(10),
                              topRight: Radius.circular(10),
                              bottomLeft: Radius.circular(10),
                              bottomRight: Radius.circular(10),
                            ),
                            image: DecorationImage(
                              fit: BoxFit.fill,
                              image: NetworkImage("https://www.picclickimg.com/d/l400/pict/265534858648_/Reproduction-Chet-Baker-Sings-Album-Cover-Poster-Size.jpg"),
                            ),
                          ),
                          // color: Colors.purple[600],
                          // child: const Center(child: Text('Item 1', style: TextStyle(fontSize: 18, color: Colors.white),)),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 10, top: 10, right: 10),
                          width: 170,
                          decoration: BoxDecoration(
                            color: Colors.green,
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(10),
                              topRight: Radius.circular(10),
                              bottomLeft: Radius.circular(10),
                              bottomRight: Radius.circular(10),
                            ),

                            image: DecorationImage(
                              fit: BoxFit.fill,
                              image: NetworkImage("https://th.bing.com/th/id/R.703d7217abd548b88fefb03017974c31?rik=w4QhaCjtQK%2bp%2fg&riu=http%3a%2f%2fcps-static.rovicorp.com%2f3%2fJPG_400%2fMI0003%2f731%2fMI0003731395.jpg%3fpartner%3dallrovi.com&ehk=rVNpQagzydpPvdQVTT0sygtuEyGVdE0gZDg4jTdGpD0%3d&risl=&pid=ImgRaw&r=0"),
                            ),
                          ),
                          // color: Colors.purple[600],
                          // child: const Center(child: Text('Item 1', style: TextStyle(fontSize: 18, color: Colors.white),)),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 10, top: 10, right: 10),
                          width: 170,
                          decoration: BoxDecoration(
                            color: Colors.green,
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(10),
                              topRight: Radius.circular(10),
                              bottomLeft: Radius.circular(10),
                              bottomRight: Radius.circular(10),
                            ),
                            image: DecorationImage(
                              fit: BoxFit.fill,
                              image: NetworkImage("https://orig00.deviantart.net/e842/f/2018/177/1/a/taeyeon_something_new_album_cover__1_by_lealbum-dceu37w.jpg"),
                            ),
                          ),
                          // color: Colors.purple[600],
                          // child: const Center(child: Text('Item 1', style: TextStyle(fontSize: 18, color: Colors.white),)),
                        ),
                        Container(

                          margin: EdgeInsets.only(left: 10, top: 10, right: 10),
                          width: 170,
                          decoration: BoxDecoration(
                            color: Colors.green,
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(10),
                              topRight: Radius.circular(10),
                              bottomLeft: Radius.circular(10),
                              bottomRight: Radius.circular(10),
                            ),
                            image: DecorationImage(
                              fit: BoxFit.fill,
                              image: NetworkImage("https://i.pinimg.com/originals/45/d2/68/45d2686f0571f3072d639bcd167e771b.jpg"),
                            ),
                          ),
                          // color: Colors.purple[600],
                          // child: const Center(child: Text('Item 1', style: TextStyle(fontSize: 18, color: Colors.white),)),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 10, top: 10, right: 10),
                          width: 170,
                          decoration: BoxDecoration(
                            color: Colors.green,
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(10),
                              topRight: Radius.circular(10),
                              bottomLeft: Radius.circular(10),
                              bottomRight: Radius.circular(10),
                            ),
                            image: DecorationImage(
                              fit: BoxFit.fill,
                              image: NetworkImage("https://350927.smushcdn.com/1388247/wp-content/uploads/2022/02/Smoulder-Album-Art-PP1.jpg?size=456x456&lossy=0&strip=1&webp=1"),
                            ),
                          ),
                          // color: Colors.purple[600],
                          // child: const Center(child: Text('Item 1', style: TextStyle(fontSize: 18, color: Colors.white),)),
                        ),
                      ],
                    ),

                  ),

                  //Another Secation

                  Container(
                    margin: EdgeInsets.only(left: 15, top: 40),
                    child: Text('New Songs',
                      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
                    ),
                  ),
                  Container(
                    height: 120,
                    child: ListView(
                      scrollDirection: Axis.horizontal,
                      children: [
                        Container(
                          margin: EdgeInsets.only(left: 20, top: 15),
                          //height:200,
                          width: 100,
                          decoration: BoxDecoration(
                              color: Colors.green,
                              borderRadius: BorderRadius.circular(100),
                              image: DecorationImage(
                                fit: BoxFit.fill,
                                image: NetworkImage("https://th.bing.com/th/id/OIP.Fv3PilDRsTaIH4NOhHTRvAHaHa?pid=ImgDet&rs=1"),
                                ),
                            //more than 50% of width makes circle
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 20, top: 15),
                          //height:200,
                          width: 100,
                          decoration: BoxDecoration(
                              color: Colors.green,
                              borderRadius: BorderRadius.circular(100),
                              image: DecorationImage(
                               fit: BoxFit.fill,
                                image: NetworkImage("https://th.bing.com/th/id/R.71bd0620b7927ba628e1f01ab13ad72c?rik=iMgtH%2fkTYyjE%2fw&riu=http%3a%2f%2ffanart.tv%2ffanart%2fmusic%2f10c8a5ad-4329-475a-a550-ff3c9cf19599%2falbumcover%2fdays-of-the-new-526a78dc45904.jpg&ehk=l4ZXWZizRgl9Plr5YcWWjGnFXT%2bsfv%2fN8AwMTJ3ZFGw%3d&risl=&pid=ImgRaw&r=0"),
                                 ),
                            //more than 50% of width makes circle
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 20, top: 15),
                          //height:200,
                          width: 100,
                          decoration: BoxDecoration(
                              color: Colors.green,
                              borderRadius: BorderRadius.circular(100),
                            image: DecorationImage(
                              fit: BoxFit.fill,
                              image: NetworkImage("https://www.allkpop.com/upload/2021/01/content/102229/1610335768-2.jfif"),
                            ),
                            //more than 50% of width makes circle
                          ),
                        ),
                        Container(

                          margin: EdgeInsets.only(left: 20, top: 15),
                          //height:200,
                          width: 100,
                          decoration: BoxDecoration(
                              color: Colors.green,
                              borderRadius: BorderRadius.circular(100),
                            image: DecorationImage(
                              fit: BoxFit.fill,
                              image: NetworkImage("https://img.discogs.com/b8d9HRK5-QJov3FV65aaFHgtmqs=/fit-in/600x540/filters:strip_icc():format(jpeg):mode_rgb():quality(90)/discogs-images/R-3794853-1477945764-7685.jpeg.jpg"),
                            ),
                            //more than 50% of width makes circle
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 20, top: 15),
                          //height:200,
                          width: 100,
                          decoration: BoxDecoration(
                              color: Colors.green,
                              borderRadius: BorderRadius.circular(100),
                              image: DecorationImage(
                                fit: BoxFit.fill,
                                  image: NetworkImage("https://i.pinimg.com/originals/f0/6c/ee/f06ceeff5f9d2579aaa758441442d659.jpg"),
                                   ),
                            //more than 50% of width makes circle
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 20, top: 15),
                          //height:200,
                          width: 100,
                          decoration: BoxDecoration(
                              color: Colors.green,
                              borderRadius: BorderRadius.circular(100),
                            image: DecorationImage(
                              fit: BoxFit.fill,
                              image: NetworkImage("https://th.bing.com/th/id/R.f69d4529ca719ac1900a28793e51d79b?rik=VGZxrDNYgTdEFQ&riu=http%3a%2f%2fwww.listenherereviews.com%2fwp-content%2fuploads%2f2017%2f02%2fseether-posion-the-parish-e1487895095387.jpg&ehk=d2YjyFKlSPsSNiYrJBKH5rVNfXQ3p%2buwB7E5x89P%2f9k%3d&risl=&pid=ImgRaw&r=0"),
                            ),
                            //more than 50% of width makes circle
                          ),
                        ),

                        Container(
                          margin: EdgeInsets.only(left: 20, top: 15),
                          //height:200,
                          width: 100,
                          decoration: BoxDecoration(
                              color: Colors.green,
                              borderRadius: BorderRadius.circular(100),
                            image: DecorationImage(
                              fit: BoxFit.fill,
                              image: NetworkImage("https://images-na.ssl-images-amazon.com/images/I/518VFZO%2BkdL.jpg"),
                            ),
                            //more than 50% of width makes circle
                          ),
                        ),


                      ],
                    ),
                  ),

                ],


              )
          ),


        ],

      ),


    );
  }
}
